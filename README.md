# jsx-demo

## Overview
A demo project for showing off JSX in TypeScript. Modified from https://github.com/Microsoft/TypeScriptSamples/tree/master/jsx

## Install dependencies
```
npm install
```

## Start webpack server
```
webpack
```

