"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var Greeter = (function (_super) {
    __extends(Greeter, _super);
    function Greeter(props) {
        var _this = _super.call(this, props) || this;
        _this.props = props;
        _this.state = { isModalOpen: false };
        return _this;
    }
    //closeModal () { this.setState({isModalOpen: false}); }
    Greeter.prototype.render = function () {
        var g = this.props.greeting;
        var greeting = 'Hello';
        if (typeof g === 'string') {
            greeting = g;
        }
        else if (g) {
            greeting = g();
        }
        return React.createElement("div", null,
            greeting,
            ", ",
            this.props.whomToGreet);
        /*return (
            <Modal isOpen={this.state.isModalOpen} onRequestClose={this.closeModal}>
                <h1>Close Me With Escape Modal</h1>
                <button onClick={this.closeModal}>Close</button>
                <div>{greeting}, {this.props.whomToGreet}</div>
            </Modal>
        )*/
    };
    return Greeter;
}(React.Component));
exports.Greeter = Greeter;
