"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var React = require("react");
var $ = require("jquery");
var greeter_1 = require("./greeter");
function getRandomGreeting() {
    switch (Math.floor(Math.random() * 4)) {
        case 0: return 'Hello';
        case 1: return 'Howdy';
        case 2: return 'Greetings to you';
        case 3: return 'Hail';
    }
}
$(function () {
    var props = {
        whomToGreet: 'euri!'
    };
    React.render(React.createElement(greeter_1.Greeter, __assign({}, props)), $('#output').get(0));
    //React.render(<div>Hello world!</div>, $('#output').get(0));
});
