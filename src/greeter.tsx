import * as React from 'react';
import Modal from 'react-modal';

export interface GreeterProps extends React.Props<Greeter> {
    whomToGreet: string;
    greeting?: string | (() => string);
}

export class Greeter extends React.Component {
    props: GreeterProps;
    state;
    constructor(props) {
        super(props);
        this.props = props;
        this.state = { isModalOpen: false }
    }

    closeModal () { this.setState({isModalOpen: false}); }

    render() {
        let g = this.props.greeting;

        let greeting = 'Hello';

        if (typeof g === 'string') {
            greeting = g;
        }
        else if (g) {
            greeting = g();
        }

        return <div>{greeting}, {this.props.whomToGreet}</div>;
        /*return (
            <Modal isOpen={this.state.isModalOpen} onRequestClose={this.closeModal}>
                <h1>Close Me With Escape Modal</h1>
                <button onClick={this.closeModal}>Close</button>
                <div>{greeting}, {this.props.whomToGreet}</div>
            </Modal>
        )*/
    }
}
